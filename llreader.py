# coding: utf-8
import sys
import os
sys.path.insert(0,'/home/sroche/gitrepos/gfit2')
from readoutputs import *
import pandas as pd
import fortranformat as ff
import argparse
from pylab import *
import matplotlib as mpl
from matplotlib import cm

def get_line_data(f,reader,headers):
    line = reader.read(f.readline())
    ld = {headers[i]:elem for i,elem in enumerate(line)}
    return ld


def read_lines(v0,width,kgas,ciso,llfile,reader,headers,skiplines=0):
    freq = 0
    n = 0
    skiplines=int(skiplines)
    v1 = v0-width/2
    v2 = v0+width/2
    data = pd.DataFrame(columns=headers)
    with open(llfile,'r') as ll:
        if skiplines:
            for i in range(skiplines):
                l = ll.readline()
            n += skiplines
        while freq<v1:
            if freq>v1:
                break
            n += 1
            freq = get_line_data(ll,reader,headers)['freq']
            sys.stdout.write('\r {}     '.format(freq))
            sys.stdout.flush()
        print('\nfirst freq is at line',n)
        nd = 0
        while freq<=v2:
            ld = get_line_data(ll,reader,headers)
            freq = ld['freq']
            sys.stdout.write('\r {}     '.format(freq))
            sys.stdout.flush()
            if freq>v2:
                break
            
            if ld['kgas']==kgas and ld['ciso']==str(ciso):
                data.loc[nd] = [ld[i] for i in headers]
                nd +=1
    data['band'] = data['quantum'].apply(lambda x: x.strip()[:23].replace(' ','')[:12])
    try:
        data['line'] = data['quantum'].apply(lambda x: x[x.index('R'):x.index('R')+5] if 'R' in x else x[x.index('P'):x.index('P')+5] if 'P' in x else x[x.index('Q'):x.index('Q')+5])
    except ValueError as e:
        print(e)

    return data

def main():

    parser = argparse.ArgumentParser(description='code to read GGG atm linelist')
    parser.add_argument('llfile',help='full path to the linelist file')
    parser.add_argument('v0',type=float,help='center wavenumber')
    parser.add_argument('width',type=float,help='width of window about the center wavenumber where lines will be fetched')
    parser.add_argument('--kgas',default=2,type=int,help='gas code (2 for CO2)')
    parser.add_argument('--ciso',default=1,type=int,help='isotopologue code')
    parser.add_argument('-s','--skiplines',default=0,type=int,help='number of lines to skip when reading the linelist files, can save time, the strong CO2 band centered at 4852 cm-1 is after line 3265020')
    parser.add_argument('--band',default='2001300001',help='vibrational transition')

    args = parser.parse_args()

    reader = ff.FortranRecordReader('(i2,a1,f12.6,e10.3,10x,2f5.0,f10.4,f4.2,f8.6,a)')
    headers = ['kgas','ciso','freq','stren','abhw','sbhw','eprime','tdpbhw','pshift','quantum']

    return read_lines(args.v0,args.width,args.kgas,args.ciso,args.llfile,reader,headers,args.skiplines), args.band


def G_G(x,t,M=44.01):
    c = 2.99792458E10
    k = 1.380649E-16
    Na = 6.02214129E23
    return np.sqrt(2*Na*k*t*np.log(2)/M)*x/c


def G_L(t,p,ps,n,ga,gs,tref=296):
    return (ga*(p-ps)+gs*(ps))*(tref/t)**n


def hwhm_plot(data,band='2001300001'):
    rcParams.update({'font.size':15})
    R = 8.31446261815324
    Na = 6.02214129E23
    mav = read_mav('/export/data/scratch/gfit2_runs/gfit2_lm/aircore/sf_rms_1/grid51/sclrms2020_offdiag/oc20120114_2054/oc20120114_2054.mav')
    t = mav['Temp']
    p = mav['Pres']
    alt = mav['Height']
    pself = mav['Density']*R*t/(mav['1co2']*Na)
    pself /= 1013.25

    data = data[data['band']==band].reset_index(drop='True')
    max_stren = data['eprime'].abs().max()

    fig,ax = subplots()
    for i in range(data.shape[0]):
        hwhm_L = G_L(t,p,pself,data.loc[i].tdpbhw,data.loc[i].abhw,data.loc[i].sbhw)
        ax.plot(hwhm_L,alt,c=cm.viridis(abs(data.loc[i].eprime)/max_stren))
        hwhm_G = G_G(data.loc[i].freq,t)
        for elem in hwhm_G:
            ax.axvline(x=elem,c='red')
    grid()
    ax.set_ylabel('Altitude (km)')
    ax.set_xlabel('HWHM ($cm^{-1}$)')
    sm = cm.ScalarMappable(cmap=cm.viridis,norm=mpl.pyplot.Normalize(0,1))
    colorbar(sm,label="Normalized E''")
    ax.set_ylim(0,30)
    ax.set_title('({})-({})'.format(band[:5],band[5:]))
    ax.set_xlim(-0.005,0.12)
    tight_layout()


if __name__=="__main__":
    d, band = main()