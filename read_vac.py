"""
Read the VAC (volume absorption coefficient) file written by GGG's abscoj.f
"""

import fortranformat as ff
import numpy as np
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="full path to input file")
    args = parser.parse_args()

    read_vac(args)


def read_vac(infile):
    reader = ff.FortranRecordReader("(f14.6,8e12.5)")
    with open(infile, "r") as f:
        for i, line in enumerate(f):
            if i > 1:
                line_data = reader.read(line)
                if line_data[0] != 0:
                    add_j = 0
                else:
                    add_j += len(line_data)
                for j, key in enumerate(header[add_j : add_j + len(line_data)]):
                    data[key] = np.append(data[key], [line_data[j]])
            elif i == 0:
                nhead, ncol = [int(i) for i in line.split()]
            elif i == 1:
                header = line.split()
                data = {key: np.array([], dtype=float) for key in header}
    return data


if __name__ == "__main__":
    main()
