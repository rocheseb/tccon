"""
Make a an interactive time series of Solar Zenith Angle at a given location and for a given time period.

This code will only work with bokeh 2.3.0 installed

For usage run:

python SZA_plot.py -h

By default it makes a plot for the location of PEARL if only a start and end date are given.
"""

from bokeh import events
from bokeh.plotting import figure
from bokeh.models import CustomJS, ColumnDataSource, Button, Div
from bokeh.layouts import gridplot
from bokeh.resources import CDN
from bokeh.embed import file_html

import numpy as np
from datetime import datetime, timedelta

import ephem

import argparse

def sun_angles(date,lat,lon,alt,pres=0,temp=0):
    """
    Inputs:
        - date: datetime object
        - lat: geodetic latitude (radians)
        - lon: longitude (radians)
        - alt: altitude (meters)
        - (optional) pres: surface pressure (mbar)
        - (optional) temp: surface temperature (Celcius)
    Outputs:
        - corrected_sza: solar zenith angle (radians)
        - azim: solar azimuth angle (radians)
    """

    # setup observer at date,lat,lon,alt
    obs = ephem.Observer()
    obs.date = date
    obs.lon = lon
    obs.lat = lat
    obs.elevation = alt
    # if surface pressure and temperature are provided for date,lat,lon then atmospheric refraction will also be taken into account when computing the sun position
    obs.pressure = pres # when set to 0 it will not be used
    obs.temp = temp

    # setup sun relative to the observer
    sun = ephem.Sun()
    sun.compute(obs)
    # this includes some corrections (e.g. parallax); see: https://rhodesmill.org/pyephem/radec
    corrected_sza= 0.5*np.pi-sun.alt    # solar zenith angle (radians)
    azim = sun.az   # azimuth angle (radians)

    return corrected_sza,azim

def main():

    parser =argparse.ArgumentParser()

    parser.add_argument('start',type=lambda s: datetime.strptime(s, '%Y%m%d'),help='start date (YYYYMMDD)')
    parser.add_argument('end',type=lambda s: datetime.strptime(s, '%Y%m%d'),help='end date (YYYYMMDD)')
    parser.add_argument('--lat',default=80.05,type=float,help='latitude in degrees north')
    parser.add_argument('--lon',default=273.58,type=float,help='longitude from 0-360 degrees')
    parser.add_argument('--alt',default=600,type=float,help='altitude in meters')
    parser.add_argument('--save',default='SZA.html',help='full path to the final .html file')
    parser.add_argument('--ylim',nargs=2,type=float,default=(55,130),help='limits for the SZA axis')
    parser.add_argument('--title',default='Solar Zenith Angle at PEARL, Eureka, Canada',help='plot title')

    args = parser.parse_args()

    fig = figure(title=args.title,plot_width=800,plot_height=400,x_axis_type='datetime')
    fig.y_range.bounds = args.ylim
    fig.y_range.start = args.ylim[1]
    fig.y_range.end = args.ylim[0]
    fig.y_range.flipped = True
    fig.xaxis.axis_label = 'Time (UTC)'
    fig.yaxis.axis_label = 'SZA (degrees)'

    dates = np.arange(args.start,args.end,timedelta(minutes=5)).astype(datetime)
    fig.x_range.bounds = (dates[0],dates[-1])
    fig.x_range.start, fig.x_range.end = dates[0], dates[-1]
    sza = np.array([np.rad2deg(sun_angles(i,np.deg2rad(args.lat),np.deg2rad(args.lon),args.alt)[0]) for i in dates])

    source = ColumnDataSource({'x':dates,'y':sza})

    area1 = fig.varea(dates,y1=108,y2=args.ylim[1],color='midnightblue',alpha=0.7)
    area2 = fig.varea(dates,y1=102,y2=108,color='darkslateblue',alpha=0.6)
    area3 = fig.varea(dates,y1=96,y2=102,color='mediumslateblue',alpha=0.6)
    area4 = fig.varea(dates,y1=90,y2=96,color='darkgrey',alpha=0.5)
    area5 = fig.varea(dates,y1=args.ylim[0],y2=90,color='yellow',alpha=0.5)

    colo_dict = {
        "Day":"rgba(255,255,0,0.5)",
        "Civil Twilight":"rgba(169,169,169,0.5)",
        "Nautical twilight":"rgba(123,104,238,0.6)",
        "Astronomical twilight":"rgba(72,61,139,0.6)",
        "Night":"rgba(25,25,112,0.7)"
        }

    info_div = Div(text=''.join(['<p style="margin:0;padding:0;text-align:center;background-color:{};color:black;font-size:18px;font-weight:bold">{}</p>'.format(val,key) for key,val in colo_dict.items()]))

    line = fig.line('x','y',color='black',line_width=2,source=source)
    
    sun_code = """
    function update_sun() {
        var now = new Date().getTime();
        var sun_data = sun_source.data;
        var startdate = source.data["x"][0];
        var i = Math.round((now-startdate)/(5*60*1000));

        sun_data["x"] = [source.data["x"][i]];
        sun_data["y"] = [source.data["y"][i]];
       
        console.log("Updated sun position");

        sun_source.change.emit();
    };

    sun_button.disabled = true;
    sun_button.visible = false;
    div.text = "The sun position will be updated every 5 minutes";

    update_sun();
    setInterval(update_sun,300000);
    """

    now = datetime.utcnow()
    if dates[0]<now<dates[-1]:
        sun_source = ColumnDataSource({'x':np.array([now]),'y':np.array([np.rad2deg(sun_angles(now,np.deg2rad(args.lat),np.deg2rad(args.lon),args.alt)[0])])})
        sun = fig.scatter('x','y',marker='star',size=35,fill_color='gold',angle=30,line_color='black',source=sun_source)
        sun_button = Button(width=300,label='Start Real Time Sun Tracking')
        div = Div(text="")
        sun_button.js_on_event(events.ButtonClick,CustomJS(args={'div':div,'sun_button':sun_button,'source':source,'sun_source':sun_source},code=sun_code))
        grid = gridplot([[fig],[div],[sun_button],[info_div]],toolbar_location='right')
    else:
        grid = gridplot([[fig],[info_div]],toolbar_location='right')
    with open(args.save,'w') as outfile:
        outfile.write(file_html(grid,CDN,'SZA'))

if __name__=="__main__":
    main()
